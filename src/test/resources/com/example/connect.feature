Feature: Can connect and receive initial data

  Scenario: Connect
    Given a running colqo server on port "8080"
    When sending message "{'token':'aloha'}" to "connect.dummy"
    Then a message is received and saved as "connect-1"
    And a message is received and saved as "connect-2"
    And message "connect-1" contains "Dean"
    And message "connect-2" contains "Test community"
