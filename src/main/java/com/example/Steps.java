package com.example;

import cucumber.api.*;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import java.util.Queue;
import java.util.Map;
import java.util.HashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.net.URI;
import static org.junit.Assert.*;
import java.io.FileOutputStream;

public class Steps {

	String output_dir = "../colqo/test-resources/integration-output/";

	private void writeOut(String name,String content){
		try {
			FileOutputStream f = new FileOutputStream(output_dir+name);
			f.write(content.getBytes());
			f.flush();
			f.close();
		} catch (Exception e){
			e.printStackTrace();
		}
	}

	static LinkedBlockingQueue<String> queue = new LinkedBlockingQueue();
	static Map<String,String> msgs = new HashMap<String,String>();

	public static class StepClient extends WebSocketClient {
		public StepClient(String url){
			super(URI.create(url));
		}
		public void onOpen( ServerHandshake handshakedata ) {};
		public void onMessage( String message ) { 
			System.out.println("Received "+message);
			if(!"o".equals(message))
				queue.add(message);
		};
		public void onClose( int code, String reason, boolean remote ){};
		public void onError( Exception ex ){ ex.printStackTrace();};
	}

StepClient client;



@Given("^a running colqo server on port \"([^\"]*)\"$")
public void a_running_colqo_server_on_port(String port) throws Throwable {
	client = new StepClient("ws://localhost:"+port+"/eventbus/109/736gl6le/websocket");
	client.connectBlocking();
	System.out.println("Connected on port "+port);
}

@When("^sending message \"([^\"]*)\" to \"([^\"]*)\"$")
public void sending_message_to(String content, String address) throws Throwable {
	String msg = ("[\"{'type':'send','address':'"+
                     address+ "','body':" +content+ "}\"]").replace("'","\\\"");
	System.out.println("Sending "+msg);                     
	client.send(msg);
}

@Then("^a message is received and saved as \"([^\"]*)\"$")
public void a_message_is_received_and_saved_as(String name) throws Throwable {
    String message = queue.poll(5,TimeUnit.SECONDS);
	assertTrue(message!=null);    
    msgs.put(name,message);
}

@Then("^message \"([^\"]*)\" contains \"([^\"]*)\"$")
public void message_contains(String name, String content) throws Throwable {
	String message = msgs.get(name);
	assertTrue(content+" not found in "+message,message!=null && message.contains(content));
	writeOut(name,message);
}


}
